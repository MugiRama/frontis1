// src/common/gql-context.interface.ts

import { Request } from 'express';

export interface GqlContext {
  req: Request;
  user?: {
    id: number;
    email: string;
    name: string;
  };
}
