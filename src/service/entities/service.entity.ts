import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { ObjectType, Field, ID, Int } from '@nestjs/graphql';
import { IsString, IsNotEmpty, IsArray, IsOptional, MaxLength, IsInt, Min } from 'class-validator';
import { ServiceRequest } from 'src/serviceRequest/entities/service-request.entity';
import { User } from 'src/users/entities/user.entity';

@ObjectType()
@Entity()
export class Service {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  @IsString()
  @IsNotEmpty()
  @MaxLength(100)
  name: string;

  @Field()
  @Column()
  @IsString()
  @IsNotEmpty()
  @MaxLength(500)
  description: string;

  @Field()
  @Column()
  @IsString()
  @IsNotEmpty()
  category: string;

  @Field()
  @Column()
  @IsString()
  @IsNotEmpty()
  contact: string;

  @Field(() => Int)
  @Column({ default: 0 })
  @IsInt()
  @Min(0)
  rating: number;

  @Field(() => [String], { nullable: true })
  @Column('simple-array')
  @IsArray()
  @IsString({ each: true })
  @IsOptional()
  photos: string[];

  @Field(() => Int)
  @Column({ type: 'int' })
  @IsInt()
  @Min(0)
  price: number;

  @OneToMany(() => ServiceRequest, serviceRequest => serviceRequest.service)
  requests: ServiceRequest[];

  @Field(() => [String], { nullable: true })
  @Column('simple-array')
  @IsArray()
  @IsString({ each: true })
  @IsOptional()
  availableTimes: string[];

  @Field(() => User)  // Asegúrate de que este campo está definido como @Field
  @ManyToOne(() => User, user => user.services)
  user: User;
}
