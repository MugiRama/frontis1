import { UserType } from 'src/users/types/user.type';
import { ServiceRequestType } from 'src/serviceRequest/types/service-request.type';
import { ServiceType } from '../types/service.type';
import { Service } from '../entities/service.entity';

export class ServiceMapper {
  toUserType(user: any): UserType {
    return {
      id: user.id,
      name: user.name,
      email: user.email,
      rol: user.rol  // Asegúrate de que el usuario tiene un rol definido y lo incluyes aquí
    };
  }

  toServiceType(service: Service): ServiceType {
    const user = this.toUserType(service.user);

    const serviceType = new ServiceType();
    serviceType.id = service.id;
    serviceType.name = service.name;
    serviceType.description = service.description;
    serviceType.category = service.category;
    serviceType.contact = service.contact;
    serviceType.rating = service.rating;
    serviceType.photos = service.photos;
    serviceType.price = service.price;
    serviceType.availableTimes = service.availableTimes;
    serviceType.user = user;

    const requests = service.requests?.map(request => {
      const requestType = new ServiceRequestType();
      requestType.id = request.id;
      requestType.requestDate = request.requestDate.toISOString();
      requestType.requester = this.toUserType(request.requester);
      requestType.service = serviceType;
      return requestType;
    }) || [];

    serviceType.requests = requests;
    return serviceType;
  }
}
