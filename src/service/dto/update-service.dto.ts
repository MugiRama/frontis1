import { InputType, Field, Int } from '@nestjs/graphql';
import { IsString, IsArray, IsOptional, ValidateIf } from 'class-validator';

@InputType()
export class UpdateServiceDto {
  @Field(() => String, { nullable: true })
  @IsOptional()
  @IsString()
  name?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @IsString()
  description?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @IsString()
  category?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @IsString()
  contact?: string;

  @Field(() => [String], { nullable: true })
  @IsOptional()
  @IsArray()
  @ValidateIf((o) => o.photos.length > 0)
  @IsString({ each: true })
  photos?: string[];
}
