import { Service } from "../entities/service.entity";
import { ServiceType } from "./service.type";

// Esta función asume que ServiceType es un tipo o clase que necesita ser llenado
function toServiceType(service: Service): ServiceType {
    const serviceType = new ServiceType();
    serviceType.id = service.id;
    serviceType.name = service.name;
    serviceType.description = service.description;
    serviceType.category = service.category;
    serviceType.contact = service.contact;
    serviceType.rating = service.rating;
    serviceType.photos = service.photos;
    serviceType.price = service.price;
    serviceType.availableTimes = service.availableTimes;
    serviceType.user = service.user; // Asegúrate de que el modelo User está correctamente mapeado

    // Añade cualquier otro campo necesario

    return serviceType;
}
