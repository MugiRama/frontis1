import { ObjectType, Field, ID, Int } from '@nestjs/graphql';
import { UserType } from 'src/users/types/user.type';
import { ServiceRequestType } from 'src/serviceRequest/types/service-request.type';


@ObjectType()
export class ServiceType {
  @Field(() => ID)
  id: number;

  @Field()
  name: string;

  @Field()
  description: string;

  @Field()
  category: string;

  @Field()
  contact: string;

  @Field(() => Int)
  rating: number;

  @Field(() => [String], { nullable: true })
  photos: string[];

  @Field(() => Int)
  price: number;

  @Field(() => [String], { nullable: true })
  availableTimes: string[];

  @Field(() => UserType, { nullable: true })  // Cambiado a UserType si eso es lo que realmente necesitas
  user: UserType;

  @Field(() => [ServiceRequestType], { nullable: true })
  requests: ServiceRequestType[];
}
