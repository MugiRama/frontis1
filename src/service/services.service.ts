import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Service } from './entities/service.entity';
import { CreateServiceInput } from './input/createService.input';
import { SearchServicesInput } from './input/search-services.input';
import { User } from 'src/users/entities/user.entity';
import { ServiceType } from './types/service.type';
import { UpdateServiceInput } from './input/update-service.input';

function toServiceType(service: Service): ServiceType {
  const serviceType = new ServiceType();
  serviceType.id = service.id;
  serviceType.name = service.name;
  serviceType.description = service.description;
  serviceType.category = service.category;
  serviceType.contact = service.contact;
  serviceType.rating = service.rating;
  serviceType.photos = service.photos;
  serviceType.price = service.price;
  serviceType.availableTimes = service.availableTimes;
  serviceType.user = service.user; // Asegúrate de que el modelo User está correctamente mapeado

  // Añade cualquier otro campo necesario

  return serviceType;
}
@Injectable()
export class ServicesService {
  constructor(
    @InjectRepository(Service)
    private servicesRepository: Repository<Service>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) { }

  async create(createServiceInput: CreateServiceInput, userId: number): Promise<ServiceType> {
    const user = await this.userRepository.findOne({
        where: { id: userId },
        relations: ['services']
    });

    if (!user) {
        throw new NotFoundException(`User with ID ${userId} not found`);
    }

    const newService = this.servicesRepository.create(createServiceInput);
    newService.user = user;

    await this.servicesRepository.save(newService);

    // Re-fetch the service to include relations
    const savedService = await this.servicesRepository.findOne({
        where: { id: newService.id },
        relations: ['user', 'requests']
    });

    if (!savedService) {
        throw new NotFoundException(`Service with ID ${newService.id} not found`);
    }

    return toServiceType(savedService);
}

  async findAll(): Promise<Service[]> {
    return this.servicesRepository.find({
      relations: ['user']  // Asegúrate de incluir esto
    });
  }

  async findOne(id: number): Promise<Service> {
    console.log('ID recibido en el servicio:', id); // Log para el ID recibido
    try {
      const service = await this.servicesRepository.findOne({
        where: { id },
        relations: ['user', 'requests'],
      });
      console.log('Resultado de la consulta:', service); // Log para el resultado de la consulta
      if (!service) {
        throw new NotFoundException(`Service with ID ${id} not found`);
      }
      return service;
    } catch (error) {
      console.error('Error en el servicio al buscar el servicio:', error);
      throw error;
    }
  }
  async findAllByUser(userId: number): Promise<Service[]> {
    const services = await this.servicesRepository.find({
      where: { user: { id: userId } },
      relations: ['user', 'requests'],
    });
    return services;
  }

  async update(serviceId: number, updateServiceInput: UpdateServiceInput, userId: number): Promise<Service> {
    // Buscar el servicio asegurándose que pertenece al usuario
    const service = await this.servicesRepository.findOne({
      where: { id: serviceId, user: { id: userId } },  // Asegurarse de que el usuario tenga acceso al servicio
      relations: ['user'],
    });

    if (!service) {
      throw new NotFoundException(`Service with ID ${serviceId} not found or user not authorized`);
    }

    // Aplicar las actualizaciones a las propiedades del servicio
    this.servicesRepository.merge(service, updateServiceInput);

    // Guardar el servicio actualizado
    const updatedService = await this.servicesRepository.save(service);

    return updatedService;
  }

  async remove(id: number): Promise<DeleteResult> {
    return this.servicesRepository.delete(id);
  }
  

  async searchServices(filters: SearchServicesInput): Promise<Service[]> {
    const query = this.servicesRepository.createQueryBuilder('service');

    if (filters.name) {
      query.andWhere('LOWER(service.name) LIKE LOWER(:name)', { name: `%${filters.name}%` });
    }
    if (filters.category) {
      query.andWhere('LOWER(service.category) LIKE LOWER(:category)', { category: `%${filters.category}%` });
    }
    if (filters.rating !== undefined) {
      query.andWhere('service.rating >= :rating', { rating: filters.rating });
    }

    return query.getMany();
  }

  async getSalesReport(providerId: number): Promise<any> {
    const totalSales = await this.servicesRepository
      .createQueryBuilder()
      .select("SUM(service.sales)", "totalSales")
      .where("service.providerId = :providerId", { providerId })
      .getRawOne();

    // Añade más lógica para calcular ventas por mes/año y los servicios más solicitados.

    return { totalSales, /* otros datos */ };
  }

}
