import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServicesService } from './services.service';
import { ServicesResolver } from './services.resolver';
import { Service } from './entities/service.entity';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [TypeOrmModule.forFeature([Service]),UsersModule],
  providers: [ServicesService, ServicesResolver],
  exports: [ServicesService]
})
export class ServicesModule {}
