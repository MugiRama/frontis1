import { Resolver, Query, Mutation, Args, Int, Context } from '@nestjs/graphql';
import { ServicesService } from './services.service';
import { CreateServiceInput } from './input/createService.input';
import { ServiceType } from './types/service.type';
import { NotFoundException, UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from 'src/auth/guard/gql-auth.guard';
import { SearchServicesInput } from './input/search-services.input';
import { Service } from './entities/service.entity';
import { GqlContext } from 'src/common/gql-context.interface';
import { UpdateServiceInput } from './input/update-service.input';
import { ServiceRequest } from 'src/serviceRequest/entities/service-request.entity';
import { User } from 'src/users/entities/user.entity';
import { UserType } from 'src/users/types/user.type';
import { ServiceRequestType } from 'src/serviceRequest/types/service-request.type';

@Resolver(() => ServiceType)
export class ServicesResolver {
  constructor(private readonly servicesService: ServicesService) { }

  @Mutation(() => ServiceType)
  @UseGuards(GqlAuthGuard)
  async createService(
    @Args('createServiceInput') createServiceInput: CreateServiceInput,
    @Context() context: GqlContext,
  ): Promise<ServiceType> {
    const user = context.user;
    if (!user || typeof user.id !== 'number') {
      throw new Error('Unauthorized or invalid user ID');
    }
    return this.servicesService.create(createServiceInput, user.id);
  }

  @Query(() => [ServiceType], { name: 'getAllServices' })
  getAllServices() {
    return this.servicesService.findAll();
  }

  @Query(() => [ServiceType])
  services() {
    return this.servicesService.findAll();
  }

  @Query(() => ServiceType)
  @UseGuards(GqlAuthGuard)
  async service(@Args('id', { type: () => Int }) id: number) {
    console.log('ID recibido en el resolver:', id); // Log para el ID recibido
    try {
      const service = await this.servicesService.findOne(id);
      console.log('Servicio encontrado:', service); // Log para el resultado de la consulta
      return service;
    } catch (error) {
      console.error('Error en el resolver al buscar el servicio:', error);
      throw error;
    }
  }
  @Query(() => [ServiceType])
  @UseGuards(GqlAuthGuard)
  async servicesByUser(@Args('userId', { type: () => Int }) userId: number) {
    return this.servicesService.findAllByUser(userId);
  }

  @Mutation(() => ServiceType)
  @UseGuards(GqlAuthGuard)
  async updateService(
    @Args('serviceId') serviceId: number,
    @Args('updateServiceInput') updateServiceInput: UpdateServiceInput,
    @Context() context: GqlContext,
  ): Promise<ServiceType> {
    if (!context.user) {
      throw new Error('Unauthorized or invalid user ID');
    }
    const updatedService = await this.servicesService.update(serviceId, updateServiceInput, context.user.id);
    return this.toServiceType(updatedService);
  }

  private toServiceType(service: Service): ServiceType {
    return {
      ...service,
      requests: service.requests ? service.requests.map(req => this.toServiceRequestType(req)) : [],
    };
  }

  private toServiceRequestType(request: ServiceRequest): ServiceRequestType {
    if (!request.service || !request.requester) { // Cambiado de `request.user` a `request.requester`
      throw new Error("Service and Requester must be present"); // Cambiado el mensaje de error
    }
    return {
      id: request.id,
      requestDate: request.requestDate.toISOString(),
      service: this.toServiceType(request.service),
      requester: this.toUserType(request.requester) // Cambiado de `user` a `requester`
    };
  }

  private toUserType(user: User): UserType {
    return {
      ...user,
      // Convert other necessary fields
    };
  }

  @Mutation(() => Boolean)
  async deleteService(@Args('id', { type: () => Int }) id: number): Promise<boolean> {
    try {
      await this.servicesService.remove(id);
      return true;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw new Error(error.message);
      }
      throw new Error("Internal server error");
    }
  }

  @Query(() => [Service], { name: 'searchServices' })
  searchServices(@Args('filters', { type: () => SearchServicesInput }) filters: SearchServicesInput) {
    return this.servicesService.searchServices(filters);
  }


}
