import { InputType, Field, Int } from '@nestjs/graphql';
import { IsNotEmpty, IsString, IsArray, IsOptional, IsInt, Min } from 'class-validator';

@InputType()
export class CreateServiceInput {
  @Field()
  @IsNotEmpty()
  @IsString()
  name: string;

  @Field()
  @IsNotEmpty()
  @IsString()
  description: string;

  @Field()
  @IsNotEmpty()
  @IsString()
  category: string;

  @Field()
  @IsNotEmpty()
  @IsString()
  contact: string;

  @Field(() => [String], { nullable: true })
  @IsArray()
  @IsString({ each: true })
  @IsOptional()
  photos: string[];

  @Field(() => Int)
  @IsInt()
  @Min(0)
  price: number;

  @Field(() => [String], { nullable: true })
  @IsArray()
  @IsString({ each: true })
  @IsOptional()
  availableTimes: string[]; // Campo adicional para manejar horarios disponibles
}

@InputType()
export class CreateServiceDataInput {
  @Field(() => CreateServiceInput)
  createServiceInput: CreateServiceInput;
}
