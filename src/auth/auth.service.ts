import { Injectable, BadRequestException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as bcryptjs from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/entities/user.entity';
import { LoginUserInput } from './dto/login-user.input';
import { LoginResponse } from './dto/login-response';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { RegisterDto } from './dto/register.dto';
import { ForgotPasswordInput } from './dto/forgot-password.input';
import { ResetPasswordInput } from './dto/reset-password.input';
import { v4 as uuidv4 } from 'uuid';
import { jwtConstants } from './constants/jwt.constants';

@Injectable()
export class AuthService {
  private resetTokens: Map<string, string> = new Map();

  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async register(registerDto: RegisterDto) {
    const existingUser = await this.usersService.findOneByEmail(registerDto.email);
    if (existingUser) {
      throw new BadRequestException('User already exists');
    }

    const hashedPassword = await bcryptjs.hash(registerDto.password, 10);
    const user = {
      ...registerDto,
      password: hashedPassword
    };

    await this.usersService.create(user);
  }

  async validateUser(email: string, password: string): Promise<User | undefined> {
    const user = await this.usersService.findOneByEmail(email);
    if (user && await bcryptjs.compare(password, user.password)) {
      return user;
    }
    return undefined;
  }

  async login(loginUserInput: LoginUserInput): Promise<LoginResponse> {
    const user = await this.usersService.findOneByEmail(loginUserInput.email);
    if (!user || !await bcryptjs.compare(loginUserInput.password, user.password)) {
      throw new BadRequestException('Invalid credentials');
    }
    const payload = {
      sub: user.id,
      email: user.email,
      name: user.name,
      
      // Puedes incluir más campos según sea necesario
    };
    const token = this.jwtService.sign(payload);
    return {
      access_token: token,
      user
    };
  }

  async findOneByEmail(email: string): Promise<User | undefined> {
    return this.usersService.findOneByEmail(email);
  }

  async findOne(id: number): Promise<User | undefined> {
    return this.usersService.findOne(id);
  }

  async findAllUsers(): Promise<User[]> {
    return this.usersService.findAll();
  }

  async validateUserByJwt(payload: JwtPayload): Promise<User | undefined> {
    return this.usersService.findOneById(payload.sub);
  }

  async forgotPassword(forgotPasswordInput: ForgotPasswordInput) {
    const user = await this.findOneByEmail(forgotPasswordInput.email);
    if (!user) {
      throw new BadRequestException('User not found');
    }
    const token = uuidv4();
    this.resetTokens.set(token, user.email);
    // Aquí deberías enviar el correo electrónico con el token
    console.log(`Reset token for ${user.email}: ${token}`);
  }

  async resetPassword(resetPasswordInput: ResetPasswordInput) {
    const email = this.resetTokens.get(resetPasswordInput.token);
    if (!email) {
      throw new BadRequestException('Invalid or expired reset token');
    }
    const user = await this.findOneByEmail(email);
    if (!user) {
      throw new BadRequestException('User not found');
    }
    const hashedPassword = await bcryptjs.hash(resetPasswordInput.newPassword, 10);
    user.password = hashedPassword;
    await this.usersService.update(user.id, user);
    this.resetTokens.delete(resetPasswordInput.token);
  }
}
