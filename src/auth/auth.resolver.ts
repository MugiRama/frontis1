import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { User } from 'src/users/entities/user.entity';
import { AuthService } from './auth.service';
import { LoginUserInput } from './dto/login-user.input';
import { LoginResponse } from './dto/login-response';
import { RegisterDto } from './dto/register.dto';
import { UserType } from 'src/users/types/user.type';
import { ForgotPasswordInput } from './dto/forgot-password.input';
import { ResetPasswordInput } from './dto/reset-password.input';

@Resolver(() => User)
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => UserType)
  async register(@Args('registerDto') registerDto: RegisterDto) {
    await this.authService.register(registerDto);
    return this.authService.findOneByEmail(registerDto.email);
  }

  @Mutation(() => LoginResponse)
  async login(@Args('loginUserInput') loginUserInput: LoginUserInput) {
    return this.authService.login(loginUserInput);
  }

  @Mutation(() => Boolean)
  async forgotPassword(@Args('forgotPasswordInput') forgotPasswordInput: ForgotPasswordInput) {
    await this.authService.forgotPassword(forgotPasswordInput);
    return true;
  }

  @Mutation(() => Boolean)
  async resetPassword(@Args('resetPasswordInput') resetPasswordInput: ResetPasswordInput) {
    await this.authService.resetPassword(resetPasswordInput);
    return true;
  }

  @Query(() => [User])
  async users() {
    return this.authService.findAllUsers();
  }
}
