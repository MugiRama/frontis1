import { Service } from "src/service/entities/service.entity";

export interface AuthenticatedUser {
    id: number;
    email: string;
    name: string;
    password: string; // Asegúrate de no exponer la contraseña a menos que sea absolutamente necesario
    rol: string;
    requests: Request[]; // Asume que tienes una entidad o interfaz Request definida
    services: Service[]; // Asume que tienes una entidad o interfaz Service definida
  }