import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ReviewsModule } from './reviews/reviews.module';
import { ScheduleModule } from './schedule/schedule.module';
import { TrackingModule } from './tracking/tracking.module';
import { ChatModule } from './chat/chat.module';
import { ServicesModule } from './service/services.module';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './auth/strategy/jwt.strategy';
import { join } from 'path';
import { ServiceRequestModule } from './serviceRequest/service-request.module';
import { jwtConstants } from './auth/constants/jwt.constants';
import { GqlContext } from './common/gql-context.interface';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3304,
      username: 'user_crud6',
      password: 'root6',
      database: 'db_crud6',
      autoLoadEntities: true,
      synchronize: true,
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      context: async ({ req }): Promise<GqlContext> => {
        const authorization = req.headers.authorization || '';
        const token = authorization.split(' ')[1];
        if (token) {
          const jwtService = new JwtService({
            secret: jwtConstants.secret,
          });
          try {
            const decoded = jwtService.verify(token) as any; // Asumimos que el token contiene los campos id, email y name
            if (decoded.id && decoded.email && decoded.name) {
              const user = {
                id: decoded.id,
                email: decoded.email,
                name: decoded.name
              };
              req['user'] = user;
              return { req, user };
            }
          } catch (err) {
            console.error('Error verifying token:', err);
          }
        }
        return { req };
      },
    }),
    UsersModule,
    AuthModule,
    ReviewsModule,
    ScheduleModule,
    TrackingModule,
    ChatModule,
    ServicesModule,
    ServiceRequestModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60m' },
    }),
  ],
  controllers: [AppController],
  providers: [AppService, JwtStrategy],
})
export class AppModule {}
