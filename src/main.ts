import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ValidationPipe } from "@nestjs/common";
import * as dotenv from 'dotenv';

dotenv.config();

console.log('JWT_SECRET:', process.env.JWT_SECRET); // Agrega esta línea para verificar

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true,
    transform: true,
  }));

  await app.listen(3008, () => {
    console.log(`🚀 Server ready at http://localhost:3008/graphql`);
  });
}

bootstrap();
