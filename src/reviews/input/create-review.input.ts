import { InputType, Field, Int } from '@nestjs/graphql';
import { IsNotEmpty, IsInt, Min, Max } from 'class-validator';

@InputType()
export class CreateReviewInput {
  @Field(() => String)
  @IsNotEmpty()
  comment: string;

  @Field(() => Int)
  @IsInt()
  @Min(1)
  @Max(5)
  rating: number;

  @Field(() => Int)
  @IsInt()
  serviceId: number;

  @Field(() => Int)
  @IsInt()
  userId: number;
}
