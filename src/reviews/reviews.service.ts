import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Review } from './entities/review.entity';
import { User } from '../users/entities/user.entity'; // Asegúrate de tener la ruta correcta
import { Service } from 'src/service/entities/service.entity';

@Injectable()
export class ReviewsService {
  constructor(
    @InjectRepository(Review)
    private readonly reviewRepository: Repository<Review>,
    @InjectRepository(Service)
    private readonly serviceRepository: Repository<Service>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  // async create(createReviewInput: CreateReviewInput): Promise<Review> {
  //   const service = await this.serviceRepository.findOneBy({ id: createReviewInput.serviceId });
  //   const user = await this.userRepository.findOneBy({ id: createReviewInput.userId });

  //   if (!service || !user) {
  //     throw new Error('Service or User not found');
  //   }

  //   const newReview = this.reviewRepository.create({
  //     comment: createReviewInput.comment,
  //     rating: createReviewInput.rating,
  //     service: service,
  //     user: user
  //   });

  //   return this.reviewRepository.save(newReview);
  // }

  // async findByService(serviceId: number): Promise<Review[]> {
  //   return this.reviewRepository.find({
  //     where: { service: { id: serviceId } },
  //     relations: ['service', 'user']  // Asegúrate de cargar las relaciones necesarias.
  //   });
  // }
}
