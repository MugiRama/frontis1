import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Field, ObjectType, ID, Int } from '@nestjs/graphql';
import { Service } from 'src/service/entities/service.entity';

@ObjectType()
@Entity()
export class Review {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => String)
  @Column()
  comment: string;

  @Field(() => Int)
  @Column()
  rating: number;

//   @Field(() => Service)
//   @ManyToOne(() => Service, service => service.reviews)
//   service: Service;

 
}
