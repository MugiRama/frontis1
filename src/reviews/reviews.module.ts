import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReviewsService } from './reviews.service';
import { ReviewsResolver } from './reviews.resolver';
import { Review } from './entities/review.entity';
import { User } from '../users/entities/user.entity';
import { Service } from 'src/service/entities/service.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Review, Service, User]) // Asegúrate de incluir todos los repositorios necesarios
  ],
  providers: [ReviewsService, ReviewsResolver]
})
export class ReviewsModule {}
