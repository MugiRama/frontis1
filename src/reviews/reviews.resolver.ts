import { Resolver, Mutation, Args, Query, Int } from '@nestjs/graphql';
import { Review } from './entities/review.entity';
import { ReviewsService } from './reviews.service';
import { CreateReviewInput } from './input/create-review.input';


@Resolver(() => Review)
export class ReviewsResolver {
  constructor(private readonly reviewsService: ReviewsService) {}

//   @Mutation(() => Review)
//   createReview(@Args('createReviewInput') createReviewInput: CreateReviewInput) {
//     return this.reviewsService.create(createReviewInput);
//   }

//   @Query(() => [Review])
//   getReviewsByService(@Args('serviceId', { type: () => Int }) serviceId: number) {
//     return this.reviewsService.findByService(serviceId);
//   }
}
