// src/service-request/entities/service-request.entity.ts
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { ObjectType, Field, ID } from '@nestjs/graphql';
import { User } from '../../users/entities/user.entity';
import { Service } from 'src/service/entities/service.entity';

@ObjectType()
@Entity()
export class ServiceRequest {
    @PrimaryGeneratedColumn()
    @Field(() => ID)
    id: number;

    @ManyToOne(() => User, user => user.requests) // Usuario que solicita el servicio
    @Field(() => User)
    requester: User;

    @ManyToOne(() => Service, service => service.requests) // Servicio solicitado
    @Field(() => Service)
    service: Service;

    @Column()
    @Field()
    requestDate: Date;
}

