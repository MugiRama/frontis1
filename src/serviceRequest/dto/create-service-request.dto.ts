// src/service-request/dto/create-service-request.dto.ts
import { InputType, Field, ID } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CreateServiceRequestDto {
    @Field(() => ID)
    @IsNotEmpty()
    serviceId: number;

    @Field(() => ID)
    @IsNotEmpty()
    userId: number;

    @Field()
    @IsNotEmpty()
    requestDate: Date;
}
