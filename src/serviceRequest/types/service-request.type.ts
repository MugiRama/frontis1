import { ObjectType, Field, ID } from '@nestjs/graphql';
import { ServiceType } from 'src/service/types/service.type';
import { UserType } from 'src/users/types/user.type';

@ObjectType()
export class ServiceRequestType {
  @Field(() => ID)
  id: number;

  @Field()
  requestDate: string;  // ISO string date

  @Field(() => ServiceType)
  service: ServiceType;

  @Field(() => UserType)
  requester: UserType;  // Cambiado de `user` a `requester`
}
