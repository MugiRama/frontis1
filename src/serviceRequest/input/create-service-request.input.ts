// src/service-request/inputs/create-service-request.input.ts
import { InputType, Field, ID } from '@nestjs/graphql';
import { CreateServiceRequestDto } from '../dto/create-service-request.dto';

@InputType()
export class CreateServiceRequestInput extends CreateServiceRequestDto {}
