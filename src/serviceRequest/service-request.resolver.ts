import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { ServiceRequest } from './entities/service-request.entity';
import { ServiceRequestService } from './service-request.service';
import { CreateServiceRequestInput } from './input/create-service-request.input';


@Resolver(() => ServiceRequest)
export class ServiceRequestResolver {
    constructor(private readonly serviceRequestService: ServiceRequestService) {}

    @Mutation(() => ServiceRequest)
    createServiceRequest(@Args('createServiceRequestInput') createServiceRequestInput: CreateServiceRequestInput): Promise<ServiceRequest> {
        return this.serviceRequestService.createServiceRequest(createServiceRequestInput);
    }

    @Query(() => [ServiceRequest])
    serviceRequests(): Promise<ServiceRequest[]> {
        return this.serviceRequestService.findAll();
    }
    @Query(() => [ServiceRequest])
    serviceRequestsByUser(@Args('userId', { type: () => Number }) userId: number): Promise<ServiceRequest[]> {
      return this.serviceRequestService.findAllServiceRequestsByUser(userId);
    }
}