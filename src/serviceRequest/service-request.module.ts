import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServiceRequestService } from './service-request.service';
import { ServiceRequestResolver } from './service-request.resolver';
import { ServiceRequest } from './entities/service-request.entity';
import { Service } from '../service/entities/service.entity'; // Importa Service si es necesario para las relaciones
import { User } from '../users/entities/user.entity'; // Importa User si es necesario para las relaciones

@Module({
  imports: [
    TypeOrmModule.forFeature([ServiceRequest, Service, User]) // Asegúrate de importar las entidades relacionadas si son necesarias para el repositorio
  ],
  providers: [ServiceRequestService, ServiceRequestResolver],
  exports: [ServiceRequestService] // Exporta el servicio si otras partes de tu aplicación necesitan acceder a él
})
export class ServiceRequestModule {}
