import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ServiceRequest } from './entities/service-request.entity';
import { Service } from 'src/service/entities/service.entity';
import { User } from 'src/users/entities/user.entity';
import { CreateServiceRequestDto } from './dto/create-service-request.dto';


@Injectable()
export class ServiceRequestService {
  constructor(
    @InjectRepository(ServiceRequest)
    private serviceRequestRepository: Repository<ServiceRequest>,
    @InjectRepository(Service)
    private serviceRepository: Repository<Service>,
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {}

  async createServiceRequest(createServiceRequestDto: CreateServiceRequestDto): Promise<ServiceRequest> {
    const service = await this.serviceRepository.findOneBy({ id: createServiceRequestDto.serviceId });
    if (!service) {
      throw new NotFoundException(`Service with ID ${createServiceRequestDto.serviceId} not found`);
    }

    const user = await this.userRepository.findOneBy({ id: createServiceRequestDto.userId });
    if (!user) {
      throw new NotFoundException(`User with ID ${createServiceRequestDto.userId} not found`);
    }

    const serviceRequest = this.serviceRequestRepository.create({
      ...createServiceRequestDto,
      service,
      requester: user,
      requestDate: new Date(createServiceRequestDto.requestDate)
    });

    await this.serviceRequestRepository.save(serviceRequest);
    return serviceRequest;
  }

  async findAllServiceRequests(): Promise<ServiceRequest[]> {
    const serviceRequests = await this.serviceRequestRepository.find({ relations: ["service", "requester"] });
    return serviceRequests.map(request => ({
      id: request.id,
      requester: request.requester,  // asegúrate de que requester está completamente poblado
      service: request.service,      // lo mismo aplica aquí
      requestDate: request.requestDate
    }));
  }

  
  async findAll(): Promise<ServiceRequest[]> {
      return this.serviceRequestRepository.find({ relations: ['service', 'user'] });
  }
  async findAllServiceRequestsByUser(userId: number): Promise<ServiceRequest[]> {
    return this.serviceRequestRepository.find({
      where: { requester: { id: userId } },
      relations: ['service', 'requester'],
    });
  }
}
