// src/users/users.service.ts
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { Service } from 'src/service/entities/service.entity';
import { CreateUserInput } from './input/createUser.Input';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Service)
    private servicesRepository: Repository<Service>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) { }

  async findOneByEmail(email: string): Promise<User | undefined> {
    const user = await this.userRepository.findOne({ where: { email } });
    return user || undefined;  // Devolver undefined si no se encuentra el usuario
  }

  async create(createUserInput: CreateUserInput): Promise<User> {
    const newUser = this.userRepository.create(createUserInput);
    return this.userRepository.save(newUser);
  }

  async findOne(id: number): Promise<User | undefined> {
    const user = await this.userRepository.findOne({ where: { id } });
    return user || undefined;  // Devolver undefined si no se encuentra el usuario
  }

  async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async update(id: number, userUpdates: Partial<User>): Promise<User> {
    const user = await this.userRepository.findOne({ where: { id } });
    if (!user) {
      throw new NotFoundException(`User with ID ${id} not found`);
    }
    
    // Realiza la actualización utilizando el usuario encontrado
    await this.userRepository.save({
      ...user, // Esparce el usuario existente
      ...userUpdates // Aplica las actualizaciones
    });

    // Retorna el usuario actualizado después de la operación de guardado
    return this.userRepository.findOne({ where: { id } }) as Promise<User>; // Asegura que el retorno sea `User`
  }

  async findOneById(id: number): Promise<User | undefined> {
    const user = await this.userRepository.findOne({ where: { id } });
    return user || undefined
  }
}
