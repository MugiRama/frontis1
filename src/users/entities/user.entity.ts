// src/users/entities/user.entity.ts
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { ObjectType, Field, ID } from '@nestjs/graphql';
import { ServiceRequest } from 'src/serviceRequest/entities/service-request.entity';
import { Service } from 'src/service/entities/service.entity';

@ObjectType()
@Entity()
export class User {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  name: string;

  @Field()
  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Field()
  @Column({ default: 'user' })
  rol: string;

  @OneToMany(() => ServiceRequest, serviceRequest => serviceRequest.requester)
  requests: ServiceRequest[];

  @OneToMany(() => Service, service => service.user)  // Asumiendo que esta relación está correcta
  services: Service[];
}
