import { InputType, Field, PartialType } from '@nestjs/graphql';
import { CreateUserDto } from './create-user.dto';
import { IsOptional, IsString, IsEmail } from 'class-validator';

@InputType()
export class UpdateUserDto extends PartialType(CreateUserDto) {
    @Field(() => String, { nullable: true })
    @IsOptional()
    @IsString()
    name?: string;

    @Field(() => String, { nullable: true })
    @IsOptional()
    @IsEmail()
    email?: string;

    @Field(() => String, { nullable: true })
    @IsOptional()
    @IsString()
    password?: string;

    @Field(() => String, { nullable: true })
    @IsOptional()
    @IsString()
    resetToken?: string;

    @Field(() => String, { nullable: true })
    @IsOptional()
    @IsString()
    resetTokenExpiration?: string;
}
